﻿*******************************************************************
* UE : LIFPROJET                                                  *
* projet : RC2 - Guide d'alimentation assisté par le data mining  *
*******************************************************************

****************************
* fait par                 *
*  - Johan VAN-DER-POST    *
*  - Rafik-eddine HENDJOUR *
*  - Khaled SAKKA-AMINI    *
****************************


Les commandes d'installation :
------------------------------

Seaborn : pip install seaborn
Pandas : pip install pandas
SQLAlchemy : pip install SQLAlchemy
ScikitLearn : pip install scikit-learn
Numpy : pip install numpy
PyMySQL : pip install PyMySQL
MatPlotLib : pip install matplotlib
WordCloud : pip install worldcloud
NLTk : pip install nltk

Pour télécharger les dictionnaires () pour nltk, il faut ouvrir un terminal python et entrez :
  >>> import nltk
  >>> nltk.download('punkt')
  >>> nltk.download()

Ensuite une fenêtre va apparaître et il vous faut suivre les instruction sur l'image "NLTK.png" à la racine du dossier.

#########################

MODIFIER LE FICHIER :
    - 'config/config.php' : Il contient les identifiants pour ce conencter à la base de donnée (à changer)

#########################

*************************************************************************************************************
*    INSTALLATION DE LA BASE DE DONNEE - MySQL                                                              *
*                                                                                                           *
*    Importer la base de donnée qui se trouve dans "RC2/data/bdd.sql" sur votre serveur MySQL               *
*************************************************************************************************************

Dossier PYTHON :
----------------
  - Preprocessing_Cleaning.py : Importe le fichier csv de la base de donnée, fais la première épuration des données
                                pour réduire la taille et exporte en base de donnée.
  - Preprocessing_Cleaning2.py : Récupère la base de donnée réduite pour filtrer les données intéressantes
                                 et ensuite l'exporter en base de donnée.
  - NearestNeighbors.py : Trouve les 5 produits similaires pour chaque produits de la base de donnée finales
                          et exporte le résultat dans le base de donnée.
  - ProductsMapping.py : Génère un graphique d'un cluster représentant les produits finaux avec X centroids
  - ProductsNearToCentroid.py : Génère un graphique contenant 1à produits autour de chaque centroids du cluster
  - WordCloud.py : Génère des images de mots pour chaque centroids du cluster

*************************************************************************************************************
*     Partie site web                                                                                       *
*     -----------------                                                                                     *
*     le site est fait en HTML, CSS, Php ainsi qu'un framework(bootstrap)                                   *
*     le menu du site ce trouvant en haut, se compose de 4 sous menus:                                      *
*       - Accueil: le lien renvois vers la page d'accueil                                                   *
*       - Cluster: contients les captures du cluster, wordcloud ...etc                                      *
*       - Recherche: contient une barre de recherche de produit ainsi que les produits similaires           *
*       - A propos: un récapitulatif du projet ainsi que les participant et leurs taches                    *
*                                                                                                           *
*                                                                                                           *
*     Lancement et utilisation du site:                                                                     *
*     ----------------------------------------                                                              *
*     le site est hébergé sur un serveur interne(Apache).                                                   *
*                                                                                                           *
*     pour pouvoir le tester et l'utiliser, il faut:                                                        *
*     - Installer WAMPP ou XAMPP sur votre machine                                                          *
*     - Lancer WAMPP ou XAMPP                                                                               *
*     - Cloner le projet (sur votre machine) a l'aide de la console, a l'emplacement " /opt/lampp/htdocs/ " *
*        en utilisant la commande " git clone LeLiensFournisParGitLab "                                     *
*     - Ouvrir un navigateur(de préférence Firefox).                                                        *
*     - Aller sur la page: localhost/RC2/index.php                                                          *
*************************************************************************************************************
